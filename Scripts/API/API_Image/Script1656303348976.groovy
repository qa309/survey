import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.testobject.HttpMessage
import com.kms.katalon.core.testobject.RequestObject as RequestObject
import com.kms.katalon.core.webservice.verification.WSResponseManager as WSResponseManager
import com.kms.katalon.core.testobject.impl.HttpTextBodyContent as HttpTextBodyContent

token = WS.sendRequest(findTestObject('API/client_credential'))

'store json response to variable'
def slurper = new groovy.json.JsonSlurper()

def get_token = slurper.parseText(token.getResponseBodyContent())

'define / field api'
def private_token = get_token.access_token


println('token is :' + private_token)

'show response to report in test suite'
response_token = token.getResponseText()

KeywordUtil.logInfo("$response_token")

//sent request
upload_image = WS.sendRequest(findTestObject('API/image_survey', [('token') : private_token, ('kode_survey') : kode_survey, ('status') : status
            , ('keterangan') : keterangan, ('source') : source]))

//variable  find object
//def request = (RequestObject)findTestObject('API/image_survey')

//HttpTextBodyContent body2 = request.getBodyContent()

//Variable get request body text
//String req = body2.getText()

//print log request text body
//KeywordUtil.logInfo("$req")

//show response to report in test suite
get_status_upload = upload_image.getResponseText()

//verify response compare to excel
WS.verifyElementPropertyValue(upload_image, 'error_code', '0000', FailureHandling.CONTINUE_ON_FAILURE)

//print response
KeywordUtil.logInfo("$get_status_upload")

