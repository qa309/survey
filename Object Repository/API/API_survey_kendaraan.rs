<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>API_survey_kendaraan</name>
   <tag></tag>
   <elementGuidId>533c233a-a629-4674-96f6-f5f1cffaa44a</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <connectionTimeout>0</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n  \&quot;KodeSurvey\&quot;: \&quot;${kode_survey}\&quot;,\n  \&quot;HasilSurveyKendaraan\&quot;: {\n    \&quot;DataSurvey\&quot;: {\n      \&quot;nama_maskapai\&quot;: \&quot;${nama_maskapai}\&quot;,\n      \&quot;nama_surveyor\&quot;: \&quot;ISTI\&quot;,\n      \&quot;tanggal_survey\&quot;: \&quot;${tgl_survey}\&quot;,\n      \&quot;lokasi_survey\&quot;: \&quot;Showroom\&quot;,\n      \&quot;keterangan_lokasi\&quot;: \&quot;Showroom ABC\&quot;,\n      \&quot;alamat_lokasi\&quot;: \&quot;JL. ABDUL MUIS NO.40, RW.8, PETOJO SEL., KECAMATAN GAMBIR, KOTA JAKARTA PUSAT, DAERAH KHUSUS IBUKOTA JAKARTA 10160, INDONESIA \&quot;,\n      \&quot;alasan_lokasi\&quot;: \&quot;PERMINTAAN NASABAH\&quot;,\n      \&quot;nama_tertanggung\&quot;: \&quot;TEST NOPOL\&quot;,\n      \&quot;status\&quot;: \&quot;Resurvey\&quot;,\n      \&quot;no_hp\&quot;: \&quot;08111111108\&quot;,\n      \&quot;no_tlp\&quot;: \&quot;021812324234\&quot;\n    },\n    \&quot;DataKendaraan\&quot;: {\n      \&quot;merk\&quot;: \&quot;ASTON\&quot;,\n      \&quot;jenis\&quot;: \&quot;Passenger\&quot;,\n      \&quot;type\&quot;: \&quot;MARTIN\&quot;,\n      \&quot;tahun_pembuatan\&quot;: \&quot;2021\&quot;,\n      \&quot;no_polisi\&quot;: \&quot;AB123CDE\&quot;,\n      \&quot;no_rangka\&quot;: \&quot;ABC 123\&quot;,\n      \&quot;no_mesin\&quot;: \&quot;BCA 12\&quot;,\n      \&quot;karoseri\&quot;: \&quot;Mobil Sedan\&quot;,\n      \&quot;warna\&quot;: \&quot;Hitam\&quot;,\n      \&quot;transmisi\&quot;: \&quot;M/T\&quot;,\n      \&quot;bahan_bakar\&quot;: \&quot;Bensin\&quot;\n    },\n    \&quot;SuratKendaraan\&quot;: {\n      \&quot;stnk\&quot;: {\n        \&quot;status\&quot;: \&quot;1\&quot;,\n        \&quot;keterangan\&quot;: \&quot;test\&quot;\n      },\n      \&quot;bpkb\&quot;: {\n        \&quot;status\&quot;: \&quot;1\&quot;,\n        \&quot;keterangan\&quot;: \&quot;test\&quot;\n      },\n      \&quot;formfaktur_forma_formc\&quot;: {\n        \&quot;status\&quot;: \&quot;1\&quot;,\n        \&quot;keterangan\&quot;: \&quot;test\&quot;\n      },\n      \&quot;blanko_kuitansi\&quot;: {\n        \&quot;status\&quot;: \&quot;1\&quot;,\n        \&quot;keterangan\&quot;: \&quot;test\&quot;\n      },\n      \&quot;fc_ktp_pemilik_terakhir\&quot;: {\n        \&quot;status\&quot;: \&quot;1\&quot;,\n        \&quot;keterangan\&quot;: \&quot;test\&quot;\n      },\n      \&quot;surat_rubah_bentuk\&quot;: {\n        \&quot;status\&quot;: \&quot;1\&quot;,\n        \&quot;keterangan\&quot;: \&quot;test\&quot;\n      }\n    },\n    \&quot;SurveyKendaraan\&quot;: {\n      \&quot;checklist\&quot;: {\n        \&quot;mandatory\&quot;: {\n          \&quot;bumper\&quot;: {\n            \&quot;status\&quot;: \&quot;1\&quot;,\n            \&quot;keterangan\&quot;: \&quot;test\&quot;\n          },\n          \&quot;lampu_besar\&quot;: {\n            \&quot;status\&quot;: \&quot;0\&quot;,\n            \&quot;keterangan\&quot;: \&quot;test\&quot;\n          },\n          \&quot;lampu_sign\&quot;: {\n            \&quot;status\&quot;: \&quot;0\&quot;,\n            \&quot;keterangan\&quot;: \&quot;test\&quot;\n          },\n          \&quot;grill\&quot;: {\n            \&quot;status\&quot;: \&quot;0\&quot;,\n            \&quot;keterangan\&quot;: \&quot;test\&quot;\n          },\n          \&quot;kap_mesin\&quot;: {\n            \&quot;status\&quot;: \&quot;0\&quot;,\n            \&quot;keterangan\&quot;: \&quot;test\&quot;\n          },\n          \&quot;spakboard\&quot;: {\n            \&quot;status\&quot;: \&quot;0\&quot;,\n            \&quot;keterangan\&quot;: \&quot;test\&quot;\n          },\n          \&quot;wiper\&quot;: {\n            \&quot;status\&quot;: \&quot;0\&quot;,\n            \&quot;keterangan\&quot;: \&quot;test\&quot;\n          },\n          \&quot;ban_mobil_utama\&quot;: {\n            \&quot;status\&quot;: \&quot;0\&quot;,\n            \&quot;keterangan\&quot;: \&quot;test\&quot;\n          },\n          \&quot;kaca_spion\&quot;: {\n            \&quot;status\&quot;: \&quot;0\&quot;,\n            \&quot;keterangan\&quot;: \&quot;test\&quot;\n          },\n          \&quot;dop_roda_vleg\&quot;: {\n            \&quot;status\&quot;: \&quot;0\&quot;,\n            \&quot;keterangan\&quot;: \&quot;test\&quot;\n          },\n          \&quot;ac_mobil\&quot;: {\n            \&quot;status\&quot;: \&quot;0\&quot;,\n            \&quot;keterangan\&quot;: \&quot;test\&quot;\n          },\n          \&quot;cat_mobil\&quot;: {\n            \&quot;status\&quot;: \&quot;0\&quot;,\n            \&quot;keterangan\&quot;: \&quot;test\&quot;\n          },\n          \&quot;accu\&quot;: {\n            \&quot;status\&quot;: \&quot;0\&quot;,\n            \&quot;keterangan\&quot;: \&quot;test\&quot;\n          },\n          \&quot;jok_mobil\&quot;: {\n            \&quot;status\&quot;: \&quot;0\&quot;,\n            \&quot;keterangan\&quot;: \&quot;test\&quot;\n          },\n          \&quot;ban_serep\&quot;: {\n            \&quot;status\&quot;: \&quot;0\&quot;,\n            \&quot;keterangan\&quot;: \&quot;test\&quot;\n          },\n          \&quot;atap\&quot;: {\n            \&quot;status\&quot;: \&quot;0\&quot;,\n            \&quot;keterangan\&quot;: \&quot;test\&quot;\n          },\n          \&quot;dashboard\&quot;: {\n            \&quot;status\&quot;: \&quot;0\&quot;,\n            \&quot;keterangan\&quot;: \&quot;test\&quot;\n          }\n        },\n        \&quot;optional\&quot;: {\n          \&quot;radio_tape_lcd\&quot;: {\n            \&quot;status\&quot;: \&quot;0\&quot;,\n            \&quot;keterangan\&quot;: \&quot;test\&quot;\n          },\n          \&quot;alarm_remote\&quot;: {\n            \&quot;status\&quot;: \&quot;0\&quot;,\n            \&quot;keterangan\&quot;: \&quot;test\&quot;\n          },\n          \&quot;central_lock\&quot;: {\n            \&quot;status\&quot;: \&quot;0\&quot;,\n            \&quot;keterangan\&quot;: \&quot;test\&quot;\n          },\n          \&quot;kunci_serep\&quot;: {\n            \&quot;status\&quot;: \&quot;2\&quot;,\n            \&quot;keterangan\&quot;: \&quot;test\&quot;\n          },\n          \&quot;power_window\&quot;: {\n            \&quot;status\&quot;: \&quot;0\&quot;,\n            \&quot;keterangan\&quot;: \&quot;test\&quot;\n          },\n          \&quot;electric_mirror\&quot;: {\n            \&quot;status\&quot;: \&quot;0\&quot;,\n            \&quot;keterangan\&quot;: \&quot;test\&quot;\n          },\n          \&quot;box_bak_kayu_besi\&quot;: {\n            \&quot;status\&quot;: \&quot;0\&quot;,\n            \&quot;keterangan\&quot;: \&quot;test\&quot;\n          }\n        }\n      },\n      \&quot;keterangan_tambahan\&quot;: \&quot;Mobil layak diasuransikan\&quot;,\n      \&quot;jarah_tempuh\&quot;: \&quot;200000\&quot;,\n      \&quot;kondisi_mesin_saat_starter\&quot;: \&quot;1\&quot;,\n      \&quot;kondisi_interior_mobil\&quot;: \&quot;1\&quot;,\n      \&quot;kondisi_mobil_keseluruhan\&quot;: \&quot;1\&quot;,\n      \&quot;noka_nosin_bekas_ketrikan\&quot;: \&quot;1\&quot;,\n      \&quot;penilaian_akhir_maskapai\&quot;: \&quot;1\&quot;\n    }\n  },\n  \&quot;Source\&quot;: \&quot;${source}\&quot;\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
      <webElementGuid>b9d23b13-cbc0-484d-b2a9-fa8eda545bab</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ${token}</value>
      <webElementGuid>f2eabca3-52e8-44dd-8653-54fc071326c3</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.3.0</katalonVersion>
   <maxResponseSize>0</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://api-dev.bcaf.id:8445/datasurveykendaraan</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>0</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>'UjdBVG22b1lnSwZ0y1EHMK0QYiLC'</defaultValue>
      <description></description>
      <id>2987f32a-1641-4eb2-985e-fa9990cb8274</id>
      <masked>false</masked>
      <name>token</name>
   </variables>
   <variables>
      <defaultValue>'Raksa_0000014'</defaultValue>
      <description></description>
      <id>788dbb74-4782-4da0-8625-3c7487f68f4f</id>
      <masked>false</masked>
      <name>kode_survey</name>
   </variables>
   <variables>
      <defaultValue>'2022-06-27T11:17:30'</defaultValue>
      <description></description>
      <id>49b34017-c715-4b03-9857-e49387cef9a3</id>
      <masked>false</masked>
      <name>tgl_survey</name>
   </variables>
   <variables>
      <defaultValue>'PT Raksa Pratikara'</defaultValue>
      <description></description>
      <id>458fbf16-5292-4942-8461-16a1e152eab7</id>
      <masked>false</masked>
      <name>nama_maskapai</name>
   </variables>
   <variables>
      <defaultValue>'Raksa'</defaultValue>
      <description></description>
      <id>aa601586-d2d4-4ec2-98eb-2b3ff8221000</id>
      <masked>false</masked>
      <name>source</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
